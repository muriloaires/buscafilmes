package br.com.buscafilmes.features.mymovies;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eftimoff.viewpagertransformers.TabletTransformer;

import java.util.ArrayList;
import java.util.List;

import br.com.buscafilmes.R;
import br.com.buscafilmes.features.mymovies.adapter.MoviesAdapter;
import br.com.buscafilmes.model.FullMovie;
import br.com.buscafilmes.model.dao.MovieDAO;
import br.com.buscafilmes.model.dao.MovieDAOImpl;
import br.com.buscafilmes.utils.view.adapters.SlidePagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyMoviesFragment extends Fragment {

    @BindView(R.id.view_movies_not_found)
    View viewMoviesNotFound;

    @BindView(R.id.my_movies_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.pager)
    ViewPager viewPagerSlider;

    @BindView(R.id.movie_title)
    TextView movieTitle;

    private SlidePagerAdapter mAdapter;

    private MovieDAO movieDAO = new MovieDAOImpl();
    private List<Object> myMovies;
    private MoviesAdapter myMoviesAdapter;

    public MyMoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View myMoviesView = inflater.inflate(R.layout.fragment_my_movies, container, false);
        ButterKnife.bind(this, myMoviesView);
        setUpRecyclerView();
        getActivity().setTitle(getString(R.string.my_movies));
        return myMoviesView;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadMyMovies();
    }

    private void setUpViewPager() {
        mAdapter = new SlidePagerAdapter(getContext(), myMovies, this);

        viewPagerSlider.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                movieTitle.setText(((FullMovie) myMovies.get(position)).getTitle());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPagerSlider.setAdapter(mAdapter);
        viewPagerSlider.setPageTransformer(true, new TabletTransformer());
        int pagerMiddlePosition = ((int) Math.ceil((double) myMovies.size() / 2)) - 1;
        if (pagerMiddlePosition == 0) {
            movieTitle.setText(((FullMovie) myMovies.get(0)).getTitle());
        }
        viewPagerSlider.setCurrentItem(pagerMiddlePosition);
    }

    private void setUpRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        myMovies = new ArrayList<>();
        myMoviesAdapter = new MoviesAdapter(myMovies, MoviesAdapter.FULL);
        mRecyclerView.setAdapter(myMoviesAdapter);
    }


    private void loadMyMovies() {
        myMovies.clear();
        myMovies.addAll(movieDAO.getAllMovies());
        if (myMovies.isEmpty()) {
            showMoviesNotFoundView();
        } else {
            showMoviesView();
        }
    }

    private void showMoviesView() {
        mRecyclerView.setVisibility(View.VISIBLE);
        viewMoviesNotFound.setVisibility(View.GONE);
        myMoviesAdapter.notifyDataSetChanged();
        setUpViewPager();
    }

    private void showMoviesNotFoundView() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        mRecyclerView.setVisibility(View.GONE);
        viewMoviesNotFound.setVisibility(View.VISIBLE);
    }
}
