package br.com.buscafilmes.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by murilo aires on 03/02/2017.
 */

public class Movie extends RealmObject{

    @SerializedName("Title")
    private String title;

    @SerializedName("Year")
    private String year;

    @PrimaryKey
    private String imdbID;

    @SerializedName("Type")
    private String type;

    @SerializedName("Poster")
    private String posterUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getType() {
        if(type.equals("movie")){
            return "Filme";
        }else if(type.equals("game")){
            return "Jogo";
        }else{
            return "Série";
        }
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }
}
