package br.com.buscafilmes.features.mymovies.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.buscafilmes.MoviesActivity;
import br.com.buscafilmes.R;
import br.com.buscafilmes.model.Movie;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by murilo aires on 03/02/2017.
 */
public class ShortMovieViewHolder extends RecyclerView.ViewHolder {

    private Context context;

    @BindView(R.id.poster)
    ImageView imgPoster;

    @BindView(R.id.title)
    TextView txtTitle;

    @BindView(R.id.year)
    TextView txtYear;

    @BindView(R.id.type)
    TextView txtType;

    @OnClick(R.id.content)
    public void onShortMovieClick(View view){
        ((MoviesActivity) context).showDetailActivity(movie);
    }

    private Movie movie;

    public ShortMovieViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.context = view.getContext();
    }

    public void bindView(Movie movie) {
        this.movie = movie;
        Picasso.with(context).load(movie.getPosterUrl()).placeholder(R.drawable.movie_placeholder).into(imgPoster);
        txtTitle.setText(movie.getTitle());
        String yearFormat = context.getResources().getString(R.string.movie_year);
        txtYear.setText(String.format(yearFormat, movie.getYear()));
        txtType.setText(movie.getType());
    }
}
