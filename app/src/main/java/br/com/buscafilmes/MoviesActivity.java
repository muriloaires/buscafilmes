package br.com.buscafilmes;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import br.com.buscafilmes.features.detail.MovieDetailActivity;
import br.com.buscafilmes.features.mymovies.MyMoviesFragment;
import br.com.buscafilmes.features.search.SearchFragment;
import br.com.buscafilmes.features.search.providers.MySuggestionProvider;
import br.com.buscafilmes.model.FullMovie;
import br.com.buscafilmes.model.Movie;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private SearchFragment searchFragment;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_placeholder, new MyMoviesFragment()).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getString(R.string.movie_name));
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(menuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                showSearchFragment();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                onBackPressed();
                return true;
            }
        });
        return true;
    }

    private void showSearchFragment() {
        searchFragment = SearchFragment.getInstance();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.fragment_placeholder, searchFragment);
        ft.addToBackStack(SearchFragment.class.getName());
        ft.commit();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        saveSearch(query);
        searchFragment.newSearch(query);
        return false;
    }

    private void saveSearch(String query) {
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                MySuggestionProvider.AUTHORITY, MySuggestionProvider.MODE);
        suggestions.saveRecentQuery(query, null);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    public void showDetailActivity(Movie movie) {
        showDetailActivity(movie.getImdbID(), movie.getTitle(), movie.getPosterUrl());
    }

    private void showDetailActivity(String imdbID, String title, String posterUrl) {
        Intent intent = new Intent(this, MovieDetailActivity.class);
        intent.putExtra("imdb", imdbID);
        intent.putExtra("movieTitle", title);
        intent.putExtra("poster", posterUrl);
        startActivity(intent);
    }

    public void showDetailActivity(FullMovie movie) {
        showDetailActivity(movie.getImdbID(), movie.getTitle(), movie.getPosterUrl());
    }
}
