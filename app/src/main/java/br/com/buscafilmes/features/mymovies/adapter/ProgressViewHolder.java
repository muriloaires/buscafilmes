package br.com.buscafilmes.features.mymovies.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by murilo aires on 03/02/2017.
 */
public class ProgressViewHolder extends RecyclerView.ViewHolder {
    public ProgressViewHolder(View view) {
        super(view);
    }
}
