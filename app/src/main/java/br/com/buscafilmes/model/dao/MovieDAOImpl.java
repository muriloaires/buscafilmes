package br.com.buscafilmes.model.dao;

import java.util.List;

import br.com.buscafilmes.model.FullMovie;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by murilo aires on 03/02/2017.
 */

public class MovieDAOImpl implements MovieDAO {



    @Override
    public List<FullMovie> getAllMovies() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(FullMovie.class).findAll();
    }

    @Override
    public FullMovie getMovie(String imdb) {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(FullMovie.class).equalTo("imdbID",imdb).findFirst();
    }

    @Override
    public void deleteMovie(String imdb) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<FullMovie> rows = realm.where(FullMovie.class).equalTo("imdbID",imdb).findAll();
        rows.deleteAllFromRealm();
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveMovie(FullMovie movie) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.insertOrUpdate(movie);
        realm.commitTransaction();
        realm.close();
    }
}
