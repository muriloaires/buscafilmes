package br.com.buscafilmes.controllers.api.routes;

import java.util.Map;

import br.com.buscafilmes.controllers.api.response.MultipleSearchResponse;
import br.com.buscafilmes.controllers.connection.RetrofitController;
import br.com.buscafilmes.model.FullMovie;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by murilo aires on 03/02/2017.
 */

public interface Movies {

    @GET(RetrofitController.BASE_URL)
    Call<MultipleSearchResponse> getMovies(@QueryMap(encoded = true) Map<String,String> immutableQuyery,@QueryMap Map<String,String> parameters);

    @GET(RetrofitController.BASE_URL)
    Call<FullMovie> getSingleMovie(@QueryMap Map<String,String> parameters);

}
