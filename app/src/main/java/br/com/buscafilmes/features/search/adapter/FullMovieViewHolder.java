package br.com.buscafilmes.features.search.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import br.com.buscafilmes.MoviesActivity;
import br.com.buscafilmes.R;
import br.com.buscafilmes.model.FullMovie;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by murilo aires on 03/02/2017.
 */
public class FullMovieViewHolder extends RecyclerView.ViewHolder {

    private Context context;
    @BindView(R.id.title)
    TextView txtTitle;

    @BindView(R.id.year)
    TextView txtYear;

    @BindView(R.id.director)
    TextView txtDirector;

    @BindView(R.id.runtime)
    TextView txtRuntime;

    @OnClick(R.id.rootView)
    public void showDetail(View view) {
        ((MoviesActivity) context).showDetailActivity(movie);
    }

    private FullMovie movie;


    public FullMovieViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.context = view.getContext();
    }

    public void bindView(FullMovie movie) {
        this.movie = movie;
        txtYear.setText(String.format(context.getResources().getString(R.string.movie_year), movie.getYear()));
        txtTitle.setText(movie.getTitle());
        txtDirector.setText(String.format(context.getResources().getString(R.string.movie_director), movie.getDirector()));
        txtRuntime.setText(String.format(context.getResources().getString(R.string.movie_runtime), movie.getRuntime()));

    }
}
