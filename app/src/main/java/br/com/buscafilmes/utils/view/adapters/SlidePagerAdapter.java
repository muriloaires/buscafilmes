package br.com.buscafilmes.utils.view.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.buscafilmes.MoviesActivity;
import br.com.buscafilmes.R;
import br.com.buscafilmes.features.mymovies.MyMoviesFragment;
import br.com.buscafilmes.model.FullMovie;

public class SlidePagerAdapter extends PagerAdapter {


    private final MyMoviesFragment myMoviesFragment;
    private final List<Object> movies;
    private Context mContext;

    public SlidePagerAdapter(Context context, List<Object> movies, MyMoviesFragment myMoviesFragment) {
        mContext = context;
        this.movies = movies;
        this.myMoviesFragment = myMoviesFragment;
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object instantiateItem(final ViewGroup collection, final int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.my_movie_item, collection, false);
        final FullMovie movie = (FullMovie) movies.get(position);
        final ImageView photo = (ImageView) layout.findViewById(R.id.imgFoto);


        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MoviesActivity) mContext).showDetailActivity(movie);
            }
        });


        Picasso.with(mContext).load(movie.getPosterUrl())
                .placeholder(R.drawable.movie_placeholder)
                .into(photo);
        collection.addView(layout);
        return layout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

}