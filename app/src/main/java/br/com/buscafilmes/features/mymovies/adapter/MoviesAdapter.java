package br.com.buscafilmes.features.mymovies.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.buscafilmes.R;
import br.com.buscafilmes.features.search.adapter.FullMovieViewHolder;
import br.com.buscafilmes.model.FullMovie;
import br.com.buscafilmes.model.Movie;

/**
 * Created by murilo aires on 03/02/2017.
 */

public class MoviesAdapter extends RecyclerView.Adapter {
    public static final int SHORT = 0;
    public static final int FULL = 1;
    private static final int PROGRESS = 2;
    private final List<Object> movies;
    private int adapterType;

    public MoviesAdapter(List<Object> movies, int adapterType) {
        this.movies = movies;
        this.adapterType = adapterType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder vh;
        switch (viewType) {
            case SHORT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.short_movie_item, parent, false);
                vh = new ShortMovieViewHolder(view);
                break;
            case FULL:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.full_movie_item, parent, false);
                vh = new FullMovieViewHolder(view);
                break;
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
                vh = new ProgressViewHolder(view);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (movies.get(position) != null) {
            Object movie = movies.get(position);
            if (movie instanceof FullMovie) {
                ((FullMovieViewHolder) holder).bindView((FullMovie) movie);
            } else {
                ((ShortMovieViewHolder) holder).bindView((Movie) movie);
            }
        }

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (movies.get(position) == null) {
            return PROGRESS;
        } else if (adapterType == SHORT) {
            return SHORT;
        } else {
            return FULL;
        }
    }
}
