package br.com.buscafilmes;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by murilo aires on 06/02/2017.
 */

public class BuscaFilmesApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
