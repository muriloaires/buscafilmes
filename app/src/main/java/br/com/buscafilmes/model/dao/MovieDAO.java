package br.com.buscafilmes.model.dao;

import java.util.List;

import br.com.buscafilmes.model.FullMovie;

/**
 * Created by murilo aires on 03/02/2017.
 */

public interface MovieDAO {

    List<FullMovie> getAllMovies();

    FullMovie getMovie(String imdb);

    void deleteMovie(String imdb);

    void saveMovie(FullMovie movie);
}
