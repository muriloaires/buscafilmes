package br.com.buscafilmes.features.search;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.buscafilmes.R;
import br.com.buscafilmes.controllers.api.response.MultipleSearchResponse;
import br.com.buscafilmes.controllers.api.routes.MoviesRouteImpl;
import br.com.buscafilmes.controllers.connection.SearchMovieHandler;
import br.com.buscafilmes.features.mymovies.adapter.MoviesAdapter;
import br.com.buscafilmes.model.FullMovie;
import br.com.buscafilmes.utils.view.EndlessRecyclerViewScrollListener;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements SearchMovieHandler {


    private String movieTitle;
    private int page = 1;

    public static SearchFragment getInstance() {
        return new SearchFragment();
    }

    @BindView(R.id.recycler_busca)
    RecyclerView mRecyclerView;

    private EndlessRecyclerViewScrollListener scrollListener;

    private MoviesRouteImpl moviesRoute = new MoviesRouteImpl();
    private List<Object> movies;
    private MoviesAdapter moviesAdapter;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View searchFragmentView = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, searchFragmentView);
        setUpRecyclerView();
        return searchFragmentView;
    }

    private void setUpRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(manager);
        movies = new ArrayList<>();
        moviesAdapter = new MoviesAdapter(movies, MoviesAdapter.SHORT);
        mRecyclerView.setAdapter(moviesAdapter);
        scrollListener = new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (movieTitle != null && movies.size() > 1) {
                    moviesRoute.getMovies(movieTitle, SearchFragment.this, SearchFragment.this.page);
                }
            }
        };
    }

    public void newSearch(String movieTitle) {
        this.movieTitle = movieTitle;
        scrollListener.resetState();
        movies.clear();
        movies.add(null);
        moviesAdapter.notifyDataSetChanged();
        moviesRoute.getMovies(movieTitle, this, page);
    }

    @Override
    public void onPreLoad() {

    }

    @Override
    public void onSearchSuccessful(MultipleSearchResponse response) {
        page++;
        movies.addAll(movies.size() - 1, response.getMovies());
        moviesAdapter.notifyDataSetChanged();
        mRecyclerView.addOnScrollListener(scrollListener);
    }

    @Override
    public void onNoMovieFound(MultipleSearchResponse response) {
        if (movies.size() > 1 && response.getError().equalsIgnoreCase("Movie not found!")) {
            removeProgress();
            mRecyclerView.removeOnScrollListener(scrollListener);
        }else if(response.getError().equalsIgnoreCase("Movie not found!")){
            removeProgress();
            mRecyclerView.removeOnScrollListener(scrollListener);
            Toast.makeText(getContext(), getString(R.string.no_movie_found), Toast.LENGTH_SHORT).show();
        }
    }

    private void removeProgress() {
        int progressPosition = movies.size() - 1;
        movies.remove(progressPosition);
        moviesAdapter.notifyItemRemoved(progressPosition);
    }

    @Override
    public void onSingleTitleFound(FullMovie movie) {

    }

    @Override
    public void onError() {
        removeProgress();
        Toast.makeText(getContext(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
    }

}
