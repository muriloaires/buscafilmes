package br.com.buscafilmes.controllers.api.routes;

import android.support.annotation.NonNull;

import com.google.common.collect.ImmutableBiMap;

import java.util.LinkedHashMap;
import java.util.Map;

import br.com.buscafilmes.controllers.api.response.MultipleSearchResponse;
import br.com.buscafilmes.controllers.connection.RetrofitController;
import br.com.buscafilmes.controllers.connection.SearchMovieHandler;
import br.com.buscafilmes.model.FullMovie;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by murilo aires on 03/02/2017.
 */

public class MoviesRouteImpl {
    private Map<String, String> parameters;
    public static final String TITLE_PARAMETER = "s";
    public static final String IMDB_ID_PARAMETER = "i";
    public static final String PLOT_PARAMETER = "plot";
    public static final String TYPE_PARAMETER = "type";
    public static final String DATA_TYPE_PARAMETER = "r";
    public static final String PAGE_PARAMETER = "page";

    public void getMovies(String movieTitle, final SearchMovieHandler searchMovieHandler, int page) {
        parameters = getSearchParameters(page);
        Call<MultipleSearchResponse> call = RetrofitController.getMovieRoute().getMovies(getTitleParameter(movieTitle), parameters);
        call.enqueue(new Callback<MultipleSearchResponse>() {
            @Override
            public void onResponse(Call<MultipleSearchResponse> call, Response<MultipleSearchResponse> response) {
                if (response.isSuccessful()) {
                    MultipleSearchResponse multipleSearchResponse = response.body();
                    if (multipleSearchResponse.isResponse()) {
                        searchMovieHandler.onSearchSuccessful(multipleSearchResponse);
                    } else {
                        searchMovieHandler.onNoMovieFound(multipleSearchResponse);
                    }
                } else {
                    searchMovieHandler.onError();
                }
            }

            @Override
            public void onFailure(Call<MultipleSearchResponse> call, Throwable t) {
                searchMovieHandler.onError();
            }
        });
    }

    public void getFullMovie(String imdb, final SearchMovieHandler handler) {
        handler.onPreLoad();
        parameters = getImdbParameters(imdb);
        Call<FullMovie> call = RetrofitController.getMovieRoute().getSingleMovie(parameters);
        call.enqueue(new Callback<FullMovie>() {
            @Override
            public void onResponse(Call<FullMovie> call, Response<FullMovie> response) {
                if (response.isSuccessful()) {
                    FullMovie fullMovie = response.body();
                    if (fullMovie.isResponse()) {
                        handler.onSingleTitleFound(fullMovie);
                    } else {
                        handler.onError();
                    }
                }
            }

            @Override
            public void onFailure(Call<FullMovie> call, Throwable t) {
                handler.onError();
            }
        });
    }

    private Map<String, String> getImdbParameters(String imdb) {
        Map<String, String> parameters = new LinkedHashMap<>();
        parameters.put(IMDB_ID_PARAMETER, imdb);
        parameters.put(PLOT_PARAMETER, "full");
        parameters.put(DATA_TYPE_PARAMETER, "json");
        parameters.put(TYPE_PARAMETER, "movie");
        return parameters;
    }

    @NonNull
    private ImmutableBiMap<String, String> getTitleParameter(String movieTitle) {
        return ImmutableBiMap.of(TITLE_PARAMETER, movieTitle.replace(" ", "+"));
    }

    private Map<String, String> getSearchParameters(int page) {
        Map<String, String> parameters = new LinkedHashMap<>();
        parameters.put(PAGE_PARAMETER, String.valueOf(page));
        parameters.put(DATA_TYPE_PARAMETER, "json");
        parameters.put(TYPE_PARAMETER, "movie");
        return parameters;
    }

}
