package br.com.buscafilmes.features.detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.buscafilmes.R;
import br.com.buscafilmes.controllers.api.response.MultipleSearchResponse;
import br.com.buscafilmes.controllers.api.routes.MoviesRouteImpl;
import br.com.buscafilmes.controllers.connection.SearchMovieHandler;
import br.com.buscafilmes.model.FullMovie;
import br.com.buscafilmes.model.dao.MovieDAO;
import br.com.buscafilmes.model.dao.MovieDAOImpl;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MovieDetailActivity extends AppCompatActivity implements SearchMovieHandler {

    @BindView(R.id.poster)
    ImageView imgPoster;

    @BindView(R.id.plot)
    TextView txtplot;


    @BindView(R.id.year)
    TextView txtYear;

    @BindView(R.id.runtime)
    TextView txtRuntime;

    @BindView(R.id.director)
    TextView txtDirector;

    @BindView(R.id.genre)
    TextView txtGenre;

    @BindView(R.id.actors)
    TextView txtActors;

    @BindView(R.id.writters)
    TextView txtWritters;

    @BindView(R.id.awards)
    TextView txtAwards;

    @BindView(R.id.text_fields)
    View textFields;

    @BindView(R.id.loading_view)
    View loadView;

    @BindView(R.id.try_again_view)
    View tryAgainView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.try_again_view)
    public void reload(View view) {
        loadMovieFromAPI();
    }

    @OnClick(R.id.poster)
    public void imgPosterActionClick(View view) {
        Intent intent = new Intent(this, PosterActivity.class);
        intent.putExtra("bitmap", poster);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imgPoster, "appear");
        startActivity(intent, options.toBundle());
    }

    private String imdb;
    private String movieTitle;
    private String poster;

    private MoviesRouteImpl moviesRoute = new MoviesRouteImpl();
    private FullMovie movie;
    private MovieDAO movieDao = new MovieDAOImpl();

    private boolean localMovie;
    private boolean loaded;
    private Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_home);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        this.imdb = getIntent().getStringExtra("imdb");
        this.poster = getIntent().getStringExtra("poster");
        this.movieTitle = getIntent().getStringExtra("movieTitle");
        Picasso.with(this).load(poster).placeholder(R.drawable.movie_placeholder).into(imgPoster);
        getSupportActionBar().setTitle(movieTitle);
        loadFullMovie();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (localMovie) {
            showDeleteMenu();
        } else {
            if (!loaded) {
                hideMenu();
            } else {
                showSaveMenu();
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                saveMovie();
                break;

            case R.id.delete:
                deleteMovie();
                break;

            default:
        }
        return true;
    }

    private void deleteMovie() {
        localMovie = false;
        movieDao.deleteMovie(imdb);
        invalidateOptionsMenu();
    }

    private void saveMovie() {
        localMovie = true;
        movieDao.saveMovie(movie);
        invalidateOptionsMenu();
    }

    private void showSaveMenu() {
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(true);
    }

    private void showDeleteMenu() {
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
    }

    private void hideMenu() {
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
    }


    private void loadFullMovie() {
        this.movie = movieDao.getMovie(imdb);
        if (this.movie == null) {
            localMovie = false;
            loadMovieFromAPI();
        } else {
            localMovie = true;
            textFields.setVisibility(View.VISIBLE);
            populateFields();
        }

    }

    private void populateFields() {
        txtYear.setText(String.format(getResources().getString(R.string.movie_year), movie.getYear()));
        txtplot.setText(movie.getPlot());
        txtRuntime.setText(String.format(getResources().getString(R.string.movie_runtime), movie.getRuntime()));
        txtDirector.setText(String.format(getResources().getString(R.string.movie_director), movie.getDirector()));
        txtGenre.setText(String.format(getResources().getString(R.string.movie_genre), movie.getGenre()));
        txtActors.setText(String.format(getResources().getString(R.string.movie_actors), movie.getActors()));
        txtWritters.setText(String.format(getResources().getString(R.string.movie_writters), movie.getWritter()));
        txtAwards.setText(String.format(getResources().getString(R.string.movie_awards), movie.getAwards()));

    }

    private void loadMovieFromAPI() {
        moviesRoute.getFullMovie(imdb, this);
    }

    @Override
    public void onPreLoad() {
        textFields.setVisibility(View.GONE);
        loadView.setVisibility(View.VISIBLE);
        tryAgainView.setVisibility(View.GONE);
    }

    @Override
    public void onSearchSuccessful(MultipleSearchResponse response) {

    }

    @Override
    public void onNoMovieFound(MultipleSearchResponse response) {

    }

    @Override
    public void onSingleTitleFound(FullMovie movie) {
        loaded = true;
        this.movie = movie;
        populateFields();
        textFields.setVisibility(View.VISIBLE);
        loadView.setVisibility(View.GONE);
        tryAgainView.setVisibility(View.GONE);
        invalidateOptionsMenu();
    }


    @Override
    public void onError() {
        tryAgainView.setVisibility(View.VISIBLE);
        loadView.setVisibility(View.GONE);
        textFields.setVisibility(View.GONE);
    }
}
