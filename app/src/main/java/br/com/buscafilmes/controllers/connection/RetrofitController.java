package br.com.buscafilmes.controllers.connection;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import br.com.buscafilmes.controllers.api.routes.Movies;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by murilo aires on 03/02/2017.
 */

public class RetrofitController {

    public static final String BASE_URL = "http://www.omdbapi.com/";

    public static Retrofit getRetrofiInstance() {
        Gson gson = new GsonBuilder().create();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(25, TimeUnit.SECONDS)
                .readTimeout(25, TimeUnit.SECONDS)
                .writeTimeout(25, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    public static Movies getMovieRoute() {
        return getRetrofiInstance().create(Movies.class);
    }

}
