package br.com.buscafilmes.controllers.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.buscafilmes.model.Movie;

/**
 * Created by murilo aires on 03/02/2017.
 */

public class MultipleSearchResponse {

    @SerializedName("Search")
    private List<Movie> movies;

    private Integer totalResults;

    @SerializedName("Response")
    private boolean response;

    @SerializedName("Error")
    private String error;

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
