package br.com.buscafilmes.controllers.connection;

import br.com.buscafilmes.controllers.api.response.MultipleSearchResponse;
import br.com.buscafilmes.model.FullMovie;

/**
 * Created by murilo aires on 03/02/2017.
 */

public interface SearchMovieHandler {

    void onPreLoad();
    void onSearchSuccessful(MultipleSearchResponse response);
    void onNoMovieFound(MultipleSearchResponse response);
    void onSingleTitleFound(FullMovie movie);
    void onError();
}
